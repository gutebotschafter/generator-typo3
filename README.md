# generator-typo3

## Generator installieren ##
`npm install -g git+ssh://git@bitbucket.org/gutebotschafter/generator-typo3.git`

## Generator ausführen ##
Per Terminal in das __document root__ auf dem Server wechseln,
dort eingeben: `yo typo3`
