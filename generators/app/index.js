var Generator = require('yeoman-generator');
var chalk = require('chalk');
var yosay = require('yosay');
var shelljs = require('shelljs');
var mkdirp = require('mkdirp');
var changeCase = require('change-case');
var git = require('simple-git');

module.exports = Generator.extend({
  initializing: function() {
    this.pkg = require('../../package.json');
    this.generateInternalExtKey = function(extKey) {
      return changeCase.pascalCase(extKey + '-Template');
    };
    this.convertExtKey = function(extKey) {
      return changeCase.snakeCase(extKey + '-Template');
    };
    this.gitInfo = {
      name: shelljs.exec('git config user.name', {silent: true}).replace(/\n/g, ''),
      email: shelljs.exec('git config user.email', {silent: true}).replace(/\n/g, ''),
    };
  },
  // Eingaben während der Installation entgegen nehmen
  prompting: function () {
    var generator = this,
      prompts = [];

    this.log(yosay(
      "Gute Botschafter GmbH\nVersion: " + this.pkg.version
    ));

    var prompts = [
    {
      name: 'key',
      message: chalk.blue('Short name of the projects/Extension-Key') + ' (abc, xyz - usually the customer code)',
      validate: function(input) {
        return (input === '') ? false : true;
      }
    },
     {
      name: 'dbuser',
      message: chalk.blue('Database') + ' Username:',
      validate: function(input) {
        return (input === '') ? false : true;
      }
    },
    {
      name: 'dbpassword',
      message: chalk.blue('Database') + ' Password:',
      validate: function(input) {
        return (input === '') ? false : true;
      }
    },
    {
      name: 'dbname',
      message: chalk.blue('Database') + ' Name:',
      validate: function(input) {
        return (input === '') ? false : true;
      }
    },
    {
      name: 'dbhost',
      message: chalk.blue('Database') + ' Host:',
      default: 'localhost',
      validate: function(input) {
        return (input === '') ? false : true;
      }
    },
    {
      name: 'dbport',
      message: chalk.blue('Database') + ' Port:',
      default: '3306',
      validate: function(input) {
        return (input === '') ? false : true;
      }
    },
    {
      name: 'dbsocket',
      message: chalk.blue('Database') + ' MySQL-Socket:',
      default: '/Applications/MAMP/tmp/mysql/mysql.sock',
      validate: function(input) {
        return (input === '') ? false : true;
      }
    },
    {
      name: 'phpVersionPath',
      message: chalk.blue('PHP: ') + 'Path to PHP version',
      default: '/Applications/MAMP/bin/php/php7.3.1/bin/php',
      validate: function(input) {
        return (input === '') ? false : true;
      }
    },
    {
      name: 'loginuser',
      message: chalk.blue('Login') + ' Username:',
      validate: function(input) {
        return (input === '') ? false : true;
      }
    },
    {
      name: 'loginpassword',
      message: chalk.blue('Login') + ' Password:',
      validate: function(input) {
        return (input === '') ? false : true;
      }
    },
    {
      name: 'name',
      message: chalk.blue('Meta information: ') + 'Name of the extension',
      validate: function(input) {
        return (input === '') ? false : true;
      }
    },
    {
      name: 'description',
      message: chalk.blue('Meta information: ') + 'Description of the extension',
    },
    {
      name: 'authorName',
      message: chalk.blue('Meta information: ') + 'Author of the extension',
      default: this.gitInfo.name,
      validate: function(input) {
        return (input === '') ? false : true;
      }
    },
    {
      name: 'authorMail',
      message: chalk.blue('Meta information: ') + 'E-mail address of the author',
      default: this.gitInfo.email,
      validate: function(input) {
        return (input === '' || /\S+@\S+\.\S+/.test(input) === false) ? false : true;
      }
    },
    {
      type: 'list',
      name: 'typo3Version',
      message: 'Which ' + chalk.bgRed.bold('TYPO3 version') + ' should be installed?',
      choices: [
          {
            name: 'TYPO3 CMS 9 LTS',
            value: '^9.5'
          },{
            name: 'TYPO3 CMS dev-master',
            value: 'dev-master'
          }]
    },
    {
      type: 'list',
      name: 'bitbucketConnection',
      message: 'Should a  ' + chalk.bgRed.bold('Bitbucket link') + ' be established?',
      choices: [
          {
            name: 'Yes',
            value: 'yes'
          },{
            name: 'No',
            value: 'no'
          }]
    },
    ];

    return this.prompt(prompts).then(function (props) {
      props.key_raw = props.key;
      props.project = props.key + '.projektstatus.de';
      props.internal_key = generator.generateInternalExtKey(props.key);
      props.folder_name = generator.convertExtKey(props.key) + '.projektstatus.de/';
      props.key = generator.convertExtKey(props.key),
      props.extBaseFolder = props.project + '/public/typo3conf/ext/';
      props.extFolder = props.extBaseFolder + props.key + '/';
      props.url = 'http://localhost/' + props.project;
      this.props = props;
    }.bind(this));
  },
  default: {
    initializeFolderStructure: function() {
      var done = this.async();
      mkdirp(this.props.project, done()); // generate root folder
      mkdirp(this.props.extBaseFolder, done()); // generate public folder into root folder
    },
    createComposerFiles: function() {
      this.fs.copyTpl(
        this.templatePath('_project-composer.json'),
        this.destinationPath(this.props.project + '/' + 'composer.json'),
        this.props
      );

      this.fs.copyTpl(
        this.templatePath('_extension-composer.json'),
        this.destinationPath(this.props.extFolder + 'composer.json'),
        this.props
      );
    },
    moveEnvAndGitignore: function() {
      var generator = this;

      var files = [
        '.project-gitignore',
        '.project-env'
      ];

      files.forEach(function(file, i, a) {
        generator.fs.copyTpl(
          generator.templatePath(file),
          generator.destinationPath(generator.props.project + '/' + file.replace('project-', '')),
          generator.props
        );
      });
    },
    createConfigLoaderFolders: function() {
      var generator = this,
          folderNames;

      folderNames = [
        'conf',
        'src',
        'var',
          'var/cache',
          'var/log'
      ];

      folderNames.forEach(function(file, i, a) {
        mkdirp(generator.props.project + '/' + file);
      });
    },
    moveConfigLoaderFiles: function() {
      var generator = this,
          files;

      files = [
        "ConfigLoader/conf/default.php",
        "ConfigLoader/conf/development.php",
        "ConfigLoader/conf/override.php",
        "ConfigLoader/conf/production.php",
        "ConfigLoader/src/ConfigLoaderFactory.php",
        "ConfigLoader/AdditionalConfiguration.php"
      ];

      files.forEach(function(file, i, a) {
        generator.fs.copy(
          generator.templatePath(file),
          generator.destinationPath(generator.props.project + '/' + file.replace('ConfigLoader/', ''))
        );
      });
    },
    createExtensionFolders: function() {
      var generator = this,
          folderNames;

      folderNames = [
        'Configuration',
          'Configuration/TypoScript',
            'Configuration/TypoScript/ContentElements',
            'Configuration/TypoScript/Page',
              'Configuration/TypoScript/Page/Config',
              'Configuration/TypoScript/Page/HeaderData',
              'Configuration/TypoScript/Page/FooterData',
              'Configuration/TypoScript/Page/IncludeCss',
              'Configuration/TypoScript/Page/IncludeJs',
              'Configuration/TypoScript/Page/Language',
              'Configuration/TypoScript/Page/Meta',
            'Configuration/TypoScript/Extensions',
          'Configuration/PageTS',
            'Configuration/PageTS/ContentElements',
            'Configuration/PageTS/Mod',
            'Configuration/PageTS/Mod/WebLayout',
            'Configuration/PageTS/Mod/WebLayout/BackendLayouts',
          'Configuration/YAML',
            'Configuration/YAML/RTE',
            'Configuration/YAML/Extensions',
              'Configuration/YAML/Extensions/Form',
          'Configuration/TCA',
            'Configuration/TCA/Overrides',
        'Resources',
          'Resources/Private/',
            'Resources/Private/Assets',
              'Resources/Private/Assets/Css',
                'Resources/Private/Assets/Css/Components',
                'Resources/Private/Assets/Css/Settings',
                'Resources/Private/Assets/Css/Mixins',
              'Resources/Private/Assets/Fonts',
              'Resources/Private/Assets/Icons',
              'Resources/Private/Assets/Images',
              'Resources/Private/Assets/JavaScript',
          'Resources/Private/Language',
          'Resources/Private/Layouts',
            'Resources/Private/Layouts/Pages',
            'Resources/Private/Layouts/Extensions',
              'Resources/Private/Layouts/Extensions/Form',
          'Resources/Private/Partials',
            'Resources/Private/Partials/Pages',
            'Resources/Private/Partials/Extensions',
              'Resources/Private/Partials/Extensions/Form',
          'Resources/Private/Templates',
            'Resources/Private/Templates/Pages',
            'Resources/Private/Templates/Extensions',
              'Resources/Private/Templates/Extensions/Form',
                'Resources/Private/Templates/Extensions/Form/Finishers',
                  'Resources/Private/Templates/Extensions/Form/Finishers/Email',
          'Resources/Public/',
            'Resources/Public/Css',
            'Resources/Public/Fonts',
            'Resources/Public/Icons',
            'Resources/Public/Images',
            'Resources/Public/JavaScript',
      ];

      /* create the main folder for this extension */
      mkdirp(generator.props.extFolder);

      /* create all other folders */
      folderNames.forEach(function(file, i, a) {
        mkdirp(generator.props.extFolder + file);
      });
    }, // createExtensionFolders
    createTYPO3Files: function() {
      var generator = this;

      TYPO3Files = [
        '_ext_emconf.php',
        '_ext_localconf.php',
        '_ext_tables.php',
        '_ext_tables.sql'
      ];

      TYPO3Files.forEach(function(file, i, a) {
        generator.fs.copyTpl(
          generator.templatePath(file),
          generator.destinationPath(generator.props.extFolder + file.replace('_', '')),
          generator.props
        );
      });
    }, // createTYPO3Files
    createTypoScriptConfigurations: function() {
      var generator = this;

      typoScriptTemplates = [
        'Configuration/PageTS/_RTE.txt',
        'Configuration/PageTS/_setup.txt',
        'Configuration/PageTS/_TCEFORM.txt',
        'Configuration/PageTS/_TCEMAIN.txt',
        'Configuration/YAML/Extensions/Form/_setup.yaml',
        'Configuration/YAML/RTE/_Custom.yaml',
        'Configuration/TypoScript/_constants.txt',
        'Configuration/TypoScript/_setup.txt',
        'Configuration/TypoScript/Page/_setup.txt',
        'Configuration/TypoScript/Page/Config/setup.txt',
        'Configuration/TypoScript/Page/HeaderData/_setup.txt',
        'Configuration/TypoScript/Page/FooterData/_setup.txt',
        'Configuration/TypoScript/Page/IncludeCss/_setup.txt',
        'Configuration/TypoScript/Page/IncludeJs/_setup.txt',
        'Configuration/TypoScript/Page/Language/setup.txt',
        'Configuration/TypoScript/Page/Meta/setup.txt'
      ];

      typoScriptTemplates.forEach(function(file, i, a) {
        generator.fs.copyTpl(
          generator.templatePath(file),
          generator.destinationPath(generator.props.extFolder + file.replace('_', '')),
          generator.props
        );
      });
    }, // createTypoScriptConfigurations
    createContentElements: function() {
      var generator = this;

      contentElementFiles = [
        'Configuration/PageTS/ContentElements/_newContentElementWizard.txt',
        'Configuration/TCA/Overrides/_tt_content.php',
        'Configuration/TypoScript/ContentElements/_Dummy.txt',
      ];

      iconFiles = [
        'Resources/Public/Icons/ce-placeholder-dummy.svg',
      ];

      contentElementFiles.forEach(function(file, i, a) {
        generator.fs.copyTpl(
          generator.templatePath(file),
          generator.destinationPath(generator.props.extFolder + file.replace('_', '')),
          generator.props
        );
      });

      iconFiles.forEach(function(file, i, a) {
        generator.fs.copyTpl(
          generator.templatePath(file),
          generator.destinationPath(generator.props.extFolder + file.replace('_', '')),
          generator.props
        );
      });
    }, // createContentElements
    createAndCopyBackendLayouts: function() {
      var generator = this;

      backendLayoutFiles = [
        'Configuration/PageTS/Mod/WebLayout/_BackendLayout.txt',
        'Configuration/PageTS/Mod/WebLayout/BackendLayouts/index.txt',
        'Configuration/PageTS/Mod/WebLayout/BackendLayouts/content.txt'
      ];

      backendLayoutFiles.forEach(function(file, i, a) {
        generator.fs.copyTpl(
          generator.templatePath(file),
          generator.destinationPath(generator.props.extFolder + file.replace('_', '')),
          generator.props
        );
      });
    }, // createAndCopyBackendLayouts
    copyTCAOverrides: function() {
      var generator = this;

      TCAOverrides = [
        'Configuration/TCA/Overrides/be_users.php',
        'Configuration/TCA/Overrides/fe_groups.php',
        'Configuration/TCA/Overrides/fe_users.php',
        'Configuration/TCA/Overrides/pages.php',
      ];

      TCAOverrides.forEach(function(file, i, a) {
        generator.fs.copy(
          generator.templatePath(file),
          generator.destinationPath(generator.props.extFolder + file)
        );
      });
    }, // copyTCAOverrides
    copyLanguageFiles: function() {
      var generator = this;

      languageFiles = [
        'Resources/Private/Language/_tt_content.xlf',
        'Resources/Private/Language/_de.tt_content.xlf',
        'Resources/Private/Language/_locallang_ce.xlf',
        'Resources/Private/Language/_de.locallang_ce.xlf',
      ];

      languageFiles.forEach(function(file, i, a) {
        generator.fs.copyTpl(
          generator.templatePath(file),
          generator.destinationPath(generator.props.extFolder + file.replace('_', '')),
          generator.props
        );
      });
    }, // copyLanguageFiles
    createHtmlFiles: function() {
      var generator = this;

      htmlFiles = [
        'Resources/Private/Layouts/Pages/_DefaultLayout.html',
        'Resources/Private/Layouts/_HeaderData.html',
        'Resources/Private/Layouts/_FooterData.html',
        'Resources/Private/Templates/Pages/_Index.html',
        'Resources/Private/Templates/Pages/_Content.html',
        'Resources/Private/Templates/Extensions/Form/Finishers/Email/_Html.html',
        'Resources/Private/Partials/Pages/_Menu.html',
      ];

      htmlFiles.forEach(function(file, i, a) {
        generator.fs.copyTpl(
          generator.templatePath(file),
          generator.destinationPath(generator.props.extFolder + file.replace('_', '')),
          generator.props
        );
      });
    }, // createHtmlFiles
    copyCssAndJsFiles: function() {
      var generator = this;

      cssAndJsFiles = [
        'Resources/Private/Assets/JavaScript/site.js',
        'Resources/Private/Assets/Css/styles.scss',
        'Resources/Private/Assets/Css/rte.scss',
        'Resources/Private/Assets/Css/Components/_fluid-styled-content.scss',
        'Resources/Private/Assets/Css/Settings/_variables.scss',
        'Resources/Private/Assets/Css/Mixins/_reset.scss',
        'Resources/Private/Assets/Css/Mixins/_fontsizefiddle.scss',
      ];

      cssAndJsFiles.forEach(function(file, i, a) {
        generator.fs.copy(
          generator.templatePath(file),
          generator.destinationPath(generator.props.extFolder + file)
        );
      });
    }, // copyCssAndJsFiles
    createJsonFiles: function() {
      var generator = this;

      jsonFiles = [
        '_package.json'
      ];

      jsonFiles.forEach(function(file, i, a) {
        generator.fs.copyTpl(
          generator.templatePath(file),
          generator.destinationPath(generator.props.extFolder + file.replace('_', '')),
          generator.props
        );
      });
    }, // createJsonFiles
    copyFiles: function() {
      var generator = this,
          configurationFiles,
          dotFiles;

      configurationFiles = [
        'gulpfile.js/index.js',
          'gulpfile.js/config/browserSync.js',
          'gulpfile.js/config/favicons.js',
          'gulpfile.js/config/fonts.js',
          'gulpfile.js/config/images.js',
          'gulpfile.js/config/release.js',
          'gulpfile.js/config/sass.js',
          'gulpfile.js/config/webpack.js',

          'gulpfile.js/lib/archiveName.js',
          'gulpfile.js/lib/compileLogger.js',
          'gulpfile.js/lib/handleErrors.js',
          'gulpfile.js/lib/prettifyTime.js',
          'gulpfile.js/lib/webpackManifest.js',

          'gulpfile.js/tasks/browserSync.js',
          'gulpfile.js/tasks/build-development.js',
          'gulpfile.js/tasks/build-production.js',
          'gulpfile.js/tasks/clean.js',
          'gulpfile.js/tasks/default.js',
          'gulpfile.js/tasks/favicons.js',
          'gulpfile.js/tasks/fonts.js',
          'gulpfile.js/tasks/icons.js',
          'gulpfile.js/tasks/images.js',
          'gulpfile.js/tasks/release.js',
          'gulpfile.js/tasks/sass.js',
          'gulpfile.js/tasks/watch.js',
          'gulpfile.js/tasks/webpack-development.js',
          'gulpfile.js/tasks/webpack-production.js',
      ];

      configurationTemplateFiles = [
        'gulpfile.js/config/_index.js'
      ];

      otherFiles = [
        'coffeelint.json',
        'ext_icon.svg',
        'Resources/Private/.htaccess',
        'Resources/Public/.htaccess'
      ];

      configurationFiles.forEach(function(file, i, a) {
        generator.fs.copy(
          generator.templatePath(file),
          generator.destinationPath(generator.props.extFolder + file)
        );
      });

      configurationTemplateFiles.forEach(function(file, i, a) {
        generator.fs.copyTpl(
          generator.templatePath(file),
          generator.destinationPath(generator.props.extFolder + file.replace('_', '')),
          generator.props
        );
      });

      otherFiles.forEach(function(file, i, a) {
        generator.fs.copy(
          generator.templatePath(file),
          generator.destinationPath(generator.props.extFolder + file)
        );
      });

      this.fs.copy(
        this.templatePath('.extension-gitignore'),
        this.destinationPath(generator.props.extFolder + '.gitignore')
      );
    }  // copyFiles
  },
  install: {
    finishHim: function() {
      var done = this.async(),
          commands;

      if (this.props.bitbucketConnection == 'true') {

        commands = [
          'cd ' + this.props.project + ' && composer config repositories.' + this.props.key + ' git git@bitbucket.org:gutebotschafter/' + this.props.key + '.git',
          'cd ' + this.props.project + ' && composer require gutebotschafter/' + this.props.key_raw + '-template:"0.0.x-dev"',
          // --------------
          'cd ' + this.props.project + ' && ' + this.props.phpVersionPath + ' composer require helhum/typo3-console',
          'cd ' + this.props.project + ' && ' + this.props.phpVersionPath + ' composer require helhum/config-loader',
          'cd ' + this.props.project + ' && ' + this.props.phpVersionPath + ' composer require helhum/dotenv-connector',
          //'cd ' + this.props.project + ' && composer require fluidtypo3/vhs',
          // --------------
          'cd ' + this.props.project + ' && chmod +x ./vendor/bin/typo3cms',
          'cd ' + this.props.project + ' && ./vendor/bin/typo3cms install:setup --non-interactive --database-user-name="' + this.props.dbuser + '" --database-user-password="' + this.props.dbpassword + '" --database-host-name="' + this.props.dbhost + '" --database-name="' + this.props.dbname + '" --database-port="' + this.props.dbport + '" --database-socket="' + this.props.dbsocket + '" --use-existing-database="true" --admin-user-name="' + this.props.loginuser + '" --admin-password="' + this.props.loginpassword + '" --site-name="TYPO3"',
          'cd ' + this.props.project + ' && ./vendor/bin/typo3cms database:updateschema "*.*"',
          'cd ' + this.props.project + ' && ./vendor/bin/typo3cms extension:setupactive ' + this.props.key + '',
          'cd ' + this.props.project + ' && ./vendor/bin/typo3cms install:generatepackagestates',
          'cd ' + this.props.project + ' && ./vendor/bin/typo3cms install:fixfolderstructure',
          // --------------
          'cd ' + this.props.project + ' && mv AdditionalConfiguration.php web/typo3conf/AdditionalConfiguration.php',
          // --------------
          'cd ' + this.props.project + ' && git add . && git commit -am "[TASK] TYPO3 Installation (Version ' + this.props.typo3Version + ')"',
          'cd ' + this.props.project + ' && git push --all',
        ];
      } else {
        commands = [
          'cd ' + this.props.project + ' && ' + this.props.phpVersionPath + ' composer require helhum/typo3-console',
          'cd ' + this.props.project + ' && ' + this.props.phpVersionPath + ' composer require helhum/config-loader',
          'cd ' + this.props.project + ' && ' + this.props.phpVersionPath + ' composer require helhum/dotenv-connector',
          //'cd ' + this.props.project + ' && composer require fluidtypo3/vhs',
          // --------------
          'cd ' + this.props.project + ' && chmod +x ./vendor/bin/typo3cms',
          'cd ' + this.props.project + ' && ./vendor/bin/typo3cms install:setup --non-interactive --database-user-name="' + this.props.dbuser + '" --database-user-password="' + this.props.dbpassword + '" --database-host-name="' + this.props.dbhost + '" --database-name="' + this.props.dbname + '" --database-port="' + this.props.dbport + '" --database-socket="' + this.props.dbsocket + '" --use-existing-database="true" --admin-user-name="' + this.props.loginuser + '" --admin-password="' + this.props.loginpassword + '" --site-name="TYPO3"',
          'cd ' + this.props.project + ' && ./vendor/bin/typo3cms database:updateschema "*.*"',
          'cd ' + this.props.project + ' && ./vendor/bin/typo3cms extension:setupactive ' + this.props.key + '',
          'cd ' + this.props.project + ' && ./vendor/bin/typo3cms install:generatepackagestates',
          'cd ' + this.props.project + ' && ./vendor/bin/typo3cms install:fixfolderstructure',
          // --------------
          'cd ' + this.props.project + ' && mv AdditionalConfiguration.php web/typo3conf/AdditionalConfiguration.php',
        ];
      }

      console.log('Bitbucket-Connection: ' + this.props.bitbucketConnection);

      if (this.props.bitbucketConnection == 'true') {
        console.log('Connect to Bitbucket');
        console.log('');
        console.log('Initializing Project Git');
        git(this.props.project)
          .init()
          .add('.')
          .commit('Initial Commit')
          .addRemote('origin', 'ssh://git@bitbucket.org/gutebotschafter/typo3-site-' + this.props.key + '.git')
          .checkoutLocalBranch('develop')
          .push(['-u', 'origin', 'master'])
          .push(['-u', 'origin', 'develop']);

        console.log('Initializing Extension Git');
        git(this.props.extFolder)
          .init()
          .add('.')
          .commit('Initial Commit')
          .addRemote('origin', 'ssh://git@bitbucket.org/gutebotschafter/' + this.props.key + '.git')
          .checkoutLocalBranch('develop')
          .push(['-u', 'origin', 'master'])
          .push(['-u', 'origin', 'develop'], function() {
            commands.forEach(function(command, i, a) {
              console.log('');
              console.log('');
              console.log('###############################');
              console.log('Command: ' + command);
              console.log('');
              shelljs.exec(command);
              console.log('###############################');
            });
            done();
          });
        } else {
          console.log('Initializing Extension');
          commands.forEach(function(command, i, a) {
            console.log('');
            console.log('');
            console.log('###############################');
            console.log('Command: ' + command);
            console.log('');
            shelljs.exec(command);
            console.log('###############################');
          });
          done();
        }
    },
  },
});
