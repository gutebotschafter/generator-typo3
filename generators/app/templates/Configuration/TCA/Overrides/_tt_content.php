<?php
/***************
 * Add Content Elements to List
 */
$backupCTypeItems = $GLOBALS['TCA']['tt_content']['columns']['CType']['config']['items'];
$GLOBALS['TCA']['tt_content']['columns']['CType']['config']['items'] = [
  [
    'LLL:EXT:<%= key %>/Resources/Private/Language/locallang_ce.xlf:content_elements',
    '--div--',
  ],
  [
    'LLL:EXT:<%= key %>/Resources/Private/Language/locallang_ce.xlf:dummy',
    '<%= key_raw %>_dummy',
    'ce-<%= key_raw %>-dummy',
  ],
];

foreach ($backupCTypeItems as $key => $value) {
  $GLOBALS['TCA']['tt_content']['columns']['CType']['config']['items'][] = $value;
}
unset($key);
unset($value);
unset($backupCTypeItems);

$tca = [
  'types' => [
    '<%= key_raw %>_dummy' => [
      'showitem' => '--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.general;general,
                      header,
                      media,
                      bodytext,
                     --div--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:tabs.access,
                     --palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.visibility;visibility,
                     --palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.access;access',
      'columnsOverrides' => [
        'bodytext' => [
          'defaultExtras' => 'richtext[*]:rte_transform[mode=ts_links]',
        ]
      ]
    ],
  ],
  'columns' => [
  ],
];

// Add content elements
$GLOBALS['TCA']['tt_content']['ctrl']['typeicon_classes'] = array_merge(
  $GLOBALS['TCA']['tt_content']['ctrl']['typeicon_classes'],
  array(
    '<%= key_raw %>_dummy' => 'ce-<%= key_raw %>-dummy',
  )
);

$GLOBALS['TCA']['tt_content'] = array_replace_recursive($GLOBALS['TCA']['tt_content'], $tca);

$GLOBALS['TCA']['tt_content']['columns']['imagewidth']['config']['range']['upper'] = 1400;
$GLOBALS['TCA']['tt_content']['columns']['fe_group']['config']['size'] = 10;
$GLOBALS['TCA']['tt_content']['columns']['fe_group']['config']['maxitems'] = 50;
