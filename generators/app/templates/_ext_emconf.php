<?php

/***************************************************************
 * Extension Manager/Repository config file for ext "<%= key %>".
 ***************************************************************/

$EM_CONF[$_EXTKEY] = [
  'title' => '<%= name %>',
  'description' => '<%= description %>',
  'category' => 'template',
  'constraints' => [
    'depends' => [
        'typo3' => '9.5.0-9.5.99',
        'rte_ckeditor' => '8.7.0-9.5.99'
    ],
  ],
  'state' => 'beta',
  'uploadfolder' => 0,
  'createDirs' => '',
  'clearCacheOnLoad' => 1,
  'author' => '<%= authorName %>',
  'author_email' => '<%= authorMail %>',
  'author_company' => 'Gute Botschafter GmbH',
  'version' => '0.000.0',
];
