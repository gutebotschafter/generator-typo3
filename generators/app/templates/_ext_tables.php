<?php
call_user_func(
  function ($extKey) {
    if (!defined('TYPO3_MODE')) {
      die('Access denied.');
    }

    TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile($extKey, 'Configuration/TypoScript', '<%= name %>');

    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::registerPageTSConfigFile(
      $extKey,
      'Configuration/PageTS/setup.txt',
      '<%= name %>'
    );

    /* Register Icons */
    if (TYPO3_MODE === 'BE') {
      /** @var \TYPO3\CMS\Core\Imaging\IconRegistry $iconRegistry */
      $iconRegistry = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Imaging\IconRegistry::class);
      /* In this array you can add content element names. The registration happens automaticly in the next step. */
      $icons = [
        'dummy'
      ];
      foreach ($icons as $icon) {
        $iconRegistry->registerIcon(
          'ce-' . $icon,
          \TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
          ['source' => 'EXT:' . $extKey . '/Resources/Public/Icons/ContentElements/' . $icon . '.svg']
        );
      }
    }
  },
  '<%= key %>'
);

/* Allow Custom Records on Standard Pages */
/*
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_xyz_slider_item');
*/
