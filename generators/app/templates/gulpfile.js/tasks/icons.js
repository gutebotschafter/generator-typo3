var browserSync = require('browser-sync');
var config = require('../config/index');
var gulp = require('gulp');

gulp.task('icons', function (cb) {
    gulp.src(config.assetDirectory + '/Icons/*')
        .pipe(gulp.dest(config.publicDirectory + '/Icons/'))
        .pipe(browserSync.reload({stream: true}));
    return cb();
});
