var gulp = require('gulp');
var gulpSequence = require('gulp-sequence').use(gulp);

gulp.task('build:production', function (cb) {
    process.env.NODE_ENV = 'production';
    gulpSequence('clean', ['fonts', 'images', 'icons'], ['sass', 'webpack:production'], 'favicons', cb);
});
