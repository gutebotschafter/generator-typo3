var path = require('path');
var paths = require('./');
var webpack = require('webpack');

module.exports = function (env) {
    var jsSrc = path.resolve(paths.assetDirectory + '/JavaScript/');
    var jsDest = path.resolve(paths.publicDirectory + '/JavaScript/');

    var webpackConfig = {
        context: jsSrc,

        plugins: [],

        resolve: {
            extensions: ['.js', '.coffee'],
            modules: [
              path.resolve(paths.assetDirectory + '/JavaScript/'),
              "node_modules"
            ],
            enforceModuleExtension: false
        },

        module: {
            loaders: [
                {
                    test: /\.js$/,
                    loader: 'babel-loader',
                    exclude: /node_modules/
                },
                {
                    test: /\.coffee$/,
                    loader: "coffee-loader",
                    exclude: /node_modules/,
                }
            ]
        },

        entry: {
            site: ['./site.js']
        },

        output: {
            path: jsDest,
            filename: '[name].js'
        },

        externals: {
            jquery: 'jQuery' // require("jquery") is external and available on the global var jQuery
        }
    };

    if (env === 'development') {
        webpackConfig.devtool = 'source-map';
        webpack.debug = true;
    }

    if (env === 'production') {
        webpackConfig.plugins.push(
            new webpack.DefinePlugin({
                'process.env': {
                    'NODE_ENV': JSON.stringify('production')
                }
            }),
            new webpack.optimize.UglifyJsPlugin()
        );
    }

    return webpackConfig;
};
