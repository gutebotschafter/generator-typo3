var path = require('path');

module.exports = {
    browserSyncTarget: '<%= url %>',
    assetDirectory: "./Resources/Private/Assets",
    sourceDirectory: "./Resources/Private",
    publicDirectory: "./Resources/Public",
};
