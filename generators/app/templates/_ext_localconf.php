<?php
if (!defined('TYPO3_MODE')) {
  die ('Access denied.');
}

/* Add default RTE configuration for <%= key %> */
$GLOBALS['TYPO3_CONF_VARS']['RTE']['Presets']['custom'] = 'EXT:<%= key %>/Configuration/RTE/Custom.yaml';
